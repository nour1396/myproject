var express = require('express')
var path = require('path')
var mongoClient = require('mongodb').MongoClient
var bp = require('body-parser')

var multer = require('multer');

var app = express()

app.use(express.static(path.join(__dirname, 'assets')))
app.use(bp.urlencoded({ extended: false }))
app.set('view engine', 'ejs')
app.set('views', 'files')

var db
mongoClient.connect('mongodb://localhost:27017/admin', { useNewUrlParser: true }, (err, client) => {
        console.log(err)
        db = client.db()
        app.listen(3000)
    })
    //multer and upload file to server
var multerConf = {
    storage: multer.diskStorage({
        destination: (req, file, next) => {
            next(null, './images');
        },
        filename: (req, file, next) => {
            var ext = file.mimetype.split('/')[1];
            next(null, file.fieldname + '-' + Date.now() + '-' + ext);

        }
    }),
    filefilter: (req, file, net) => {
        if (!file) {
            next();
        }
        var image = file.mimetype.startwidth('image/');
        if (image) {
            next(null, true);
        } else {
            next({ message: "file type not supported" }, false);
        }
    }

};
/*get home page*/
app.get('/', (req, res, next) => {
    res.render('index', { title: 'upload file' })
});
/*post home page*/
app.post('/upload', multer(multerConf).single('photo'), (req, res, next) => {
    if (req.file) {
        console.log(req.file);
        req.body.photo = req.file.filename;
    }
});

// signup
app.get('/signup', function(req, res, next) {
    res.render('signup')
})

app.post('/signup', (req, res, next) => {
    db.collection('Users').insertOne(req.body, () => {
        res.redirect('/signup')
    })
})

// filter
app.get('/filter', (req, res, next) => {
        req.query.language = req.query.language || 'All'
        req.query.continent = req.query.continent || 'All'

        var filterObject = {}
        if (req.query.language !== 'All') filterObject.language = { $regex: ".*" + req.query.language + ".*" }
        if (req.query.continent !== 'All') filterObject.continent = req.query.continent

        db.collection('list_of_countries').find(filterObject).toArray().then((countries) => {
            db.collection('list_of_languages').find({}).toArray().then((languages) => {
                res.render('filter', {
                    countries: countries,
                    languages: languages
                })
            })
        })
    })
    //data-en
app.get('/data-en', function(req, res, next) {
    res.render('data-en')
})

app.post('/data-en', (req, res, next) => {
        db.collection('newperson-en').insertOne(req.body, () => {
            res.redirect('/data-en')
        })
    })
    //data-en
app.get('/data-ar', function(req, res, next) {
    res.render('data-ar')
})

app.post('/data-ar', (req, res, next) => {
    db.collection('newperson-ar').insertOne(req.body, () => {
        res.redirect('/data-ar')
    })
})

app.get('/test-json', (req, res, next) => {
    res.render('test-json')
})

app.post('/test-json', (req, res, next) => {
    console.log(req.body)
    res.redirect('/test-json')
})